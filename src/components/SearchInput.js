import React from 'react';

export default class SearchInput extends React.Component {
    constructor(props) {
        super(props);
        this.timeout = null;
    }
    
    handleChange = (e) => {
        const { value } = e.target;
        const { onSearch } = this.props;

        if (this.timeout !== null) {
            clearTimeout(this.timeout);
            this.timeout = null;
        }

        this.timeout = setTimeout(() => {
            onSearch(value);
        }, 300);
    }

    render() {
        return (
            <input
                className="form-control"
                type="search"
                placeholder="Search"
                onChange={this.handleChange}
            />
        );
    }
}