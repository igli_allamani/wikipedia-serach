import React from 'react';

export default class Results extends React.Component {

    render() {
        const { query, data = [] } = this.props;

        if (query.length < 3) {
            return null;
        }

        if (!data.length) {
            return (
                <h3 className="h3">No Results</h3>
            );
        }

        return (
            <>
                <h3 class="h3 mt-4">Search Results for "{query}"</h3>
                <ul className="list-group mt-3">
                    {data.map((item) => {
                        return (
                            <li key={item.key} className="list-group-item">
                                <h5 className="h5">{item.title}</h5>
                                <p className="text-muted" dangerouslySetInnerHTML={{ __html: item.snippet }} />
                            </li>
                        );
                    })}
                </ul>
            </>
        );
    }
}