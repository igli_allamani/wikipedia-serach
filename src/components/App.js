import React from 'react';

import SearchInput from './SearchInput';
import SearchResults from './Results';

import { searchWiki } from '../util/getData';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      query: '',
      results: [],
    };
  }

  handleSearch = async (query) => {
    query = query.trim();
    if (query.length >= 3) {
      const results = await searchWiki(query, 5);
      this.setState({
        results,
        query,
      });
    }
  }

  render() {
    const { query, results } = this.state;

    return (
      <div className="container" style={{ maxWidth: '60%' }}>
        <h2 className="h1 my-4">Wikipedia Search</h2>
        <SearchInput onSearch={this.handleSearch} />
        <SearchResults query={query} data={results} />
      </div>
    );
  }
}

