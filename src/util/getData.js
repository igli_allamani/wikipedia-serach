export const getData = async (url) => {
    return fetch(url).then(r => r.json());
};

export const searchWiki = async (query, limit) => {
    const api = 'https://en.wikipedia.org/w/api.php?origin=*&action=query&list=search&format=json&srsearch=';
    const res = await getData(`${api}${query}`);

    if (!res.query || !res.query.search) {
        return [];
    }

    return Array.from(res.query.search).slice(0, limit).map(i => ({
        key: i.pageid,
        title: i.title,
        snippet: i.snippet,
    }));
};
